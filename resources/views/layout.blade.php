<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Canoe Project</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >

        <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
        <script src="https://www.amcharts.com/lib/3/pie.js"></script>
        <script src="https://www.amcharts.com/lib/3/serial.js"></script>
        <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    </head>
    <body class="py-5">
        <div class="header">
            <h2 class="text-center">Canoe Project</h2>
            <hr />
            <h4 class="text-center">@yield('title')</h4>
            <hr />
        </div>
        <div id="app" class="container">
            @yield('content')
        </div>
    </body>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}?v=1.7"></script>
    <script>
        var root = new Vue({
            el: '#app',
        });
    </script>
</html>
