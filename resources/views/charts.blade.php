@extends('layout')

@section('title', 'Chart Integration')

@section('content')
    <chart-component></chart-component>
@endsection