@extends('layout')

@section('title', 'User Preferences')

@section('content')
    <preferences-component></preferences-component>
@endsection