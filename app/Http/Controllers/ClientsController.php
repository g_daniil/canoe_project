<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

class ClientsController extends Controller
{
    public function get(Request $request) {
        $clients = Client::all();
        return [
            'clients' => $clients
        ];
    }

    public function getChart(Request $request) {
        $client_id = $request->client_id;

        $client = Client::findOrFail($client_id);

        $line = [
            'valueField' => 'amount',
            'categoryField' => 'date',
            'data' => []
        ];

        $cost = 0;
        $current_value = 0;
        $changes = [];
        foreach($client->investments()->orderBy('date')->get() as $inv) {
            $cost += $inv->amount;
            $current_value += $inv->accumulated_amount;
            if(!isset($changes[$inv->date])) {
                $changes[$inv->date] = 0;
            }
            $changes[$inv->date] += $inv->amount;
            $cur_amt = $inv->amount;
            foreach($inv->cashFlows()->orderBy('date')->get() as $cf) {
                $amt = ($cur_amt * $cf->return)/100;
                $cur_amt += $amt;
                if(!isset($changes[$cf->date])) {
                    $changes[$cf->date] = 0;
                }
                $changes[$cf->date] += $amt;
            }
        }

        ksort($changes);

        $amt = 0;
        $data = [];
        foreach($changes as $k => $v) {
            $amt += $v;
            $data[] = [
                'date' => $k,
                'amount' => $amt
            ];
        }

        $line['data'] = $data;

        $pie = [
            'valueField' => 'amount',
            'titleField' => 'title',
            'data' => [
                ['title' => 'Invested', 'amount' => $cost],
                ['title' => 'Gained', 'amount' => $current_value - $cost],
            ]
        ];

        return [
            'charts' => [
                'pie' => $pie,
                'line' => $line
            ]
        ];

    }
}
