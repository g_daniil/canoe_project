<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Fund;
use App\Investment;

class FundsController extends Controller
{
    public function index() {
        return view('masking');
    }

    public function get(Request $request){
        $client_id = $request->client_id;
        $client = Client::findOrFail($client_id);
        
        $with_investments = false;

        if($request->with_investments) {
            $with_investments = true;
        }

        $funds = null;
        if(!$with_investments) {
            $funds = Fund::all();
        } else {
            $funds = Fund::whereHas('investments', function($q) use ($client_id) {
                $q->where('client_id', $client_id);
            })->get();
        }

        $permissions = json_decode($client->permission);

        $canViewAll = in_array('*', $permissions);

        foreach($funds as $k => $v) {
            if(!$canViewAll && !in_array($v->type, $permissions)) {
                //mask
                
                $v->name = preg_replace('/[\S]/', '#', $v->name);
                $v->inception_date = preg_replace('/[\S]/', '#', $v->inception_date);
                $v->description = preg_replace('/[\S]/', '#', $v->description);

                $funds[$k] = $v;
            }
        }

        return ['funds' => $funds];
    }


    public function getTypes(Request $request){
        
        $fund_types = Fund::distinct()->get(['type'])->pluck('type');

        return ['fund_types' => $fund_types];
    }


    public function getChart(Request $request) {

        $fund_type = $request->fund_type;

        $line = [
            'valueField' => 'amount',
            'categoryField' => 'date',
            'data' => []
        ];

        $cost = 0;
        $current_value = 0;
        $changes = [];
        $investments = Investment::whereHas('fund', function($q) use($fund_type) {
            $q->where('type', $fund_type);
        })->orderBy('date')->get();

        foreach($investments as $inv) {
            $cost += $inv->amount;
            $current_value += $inv->accumulated_amount;
            if(!isset($changes[$inv->date])) {
                $changes[$inv->date] = 0;
            }
            $changes[$inv->date] += $inv->amount;
            $cur_amt = $inv->amount;
            foreach($inv->cashFlows()->orderBy('date')->get() as $cf) {
                $amt = ($cur_amt * $cf->return)/100;
                $cur_amt += $amt;
                if(!isset($changes[$cf->date])) {
                    $changes[$cf->date] = 0;
                }
                $changes[$cf->date] += $amt;
            }
        }

        ksort($changes);

        $amt = 0;
        $data = [];
        foreach($changes as $k => $v) {
            $amt += $v;
            $data[] = [
                'date' => $k,
                'amount' => $amt
            ];
        }

        $line['data'] = $data;

        $pie = [
            'valueField' => 'amount',
            'titleField' => 'title',
            'data' => [
                ['title' => 'Invested', 'amount' => $cost],
                ['title' => 'Gained', 'amount' => $current_value - $cost],
            ]
        ];

        return [
            'charts' => [
                'pie' => $pie,
                'line' => $line
            ]
        ];

    }
}
