<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Investment;

class InvestmentController extends Controller
{
    public function getInvestment(Request $request) {
        $client_id = $request->client_id;
        $fund_id = $request->fund_id;

        $investment = Investment::where('client_id', $client_id)->where('fund_id', $fund_id)->first();

        return ['investment'=>$investment];
    }
}
