<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Cashflow;

class CashflowController extends Controller
{
    public function index() {
        return view('cash-flow');
    }

    public function postCashFlow(Request $request) {
        $inv_id = $request->investment_id;
        $return = $request->return;

        $cf = new Cashflow();
        $cf->investment_id = $inv_id;
        $cf->return = $return;
        $cf->date = \Carbon\Carbon::now();
        $cf->save();

        return response(null, 200);

    }
}
