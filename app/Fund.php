<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fund extends Model
{
    protected $table = 'funds';

    protected $fillable = [
        'name',
        'type',
        'inception_date',
        'description'
    ];

    public function investments() {
        return $this->hasMany('App\Investment');
    }
}
