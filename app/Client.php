<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';

    protected $fillable = [
        'name',
        'permission',
        'description',
        'preference'
    ];

    protected $casts = [
        'permission' => 'array',
        'preference' => 'array'
    ];

    public function investments() {
        return $this->hasMany('App\Investment');
    }
}
