<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashFlow extends Model
{
    protected $table = 'cash_flows';

    protected $fillable = [
        'investment_id',
        'date',
        'return'
    ];

    public function investment() {
        return $this->belongsTo('App\Investment');
    }
}
