<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    protected $table = 'investments';

    protected $fillable = [
        'client_id',
        'fund_id',
        'date',
        'amount'
    ];

    protected $appends = [
        'accumulated_amount'
    ];

    public function getAccumulatedAmountAttribute() {
        $amount = $this->amount;
        foreach($this->cashFlows as $cf) {
            $amount += ($cf->return * $amount)/100;
        }
        return $amount;
    }

    public function client() {
        return $this->belongsTo('App\Client');
    }

    public function fund() {
        return $this->belongsTo('App\Fund');
    }

    public function cashFlows() {
        return $this->hasMany('App\CashFlow');
    }
}
