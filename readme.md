1. You will need to add a MYSQL database 'canoe' with user 'canoe' and no password.
2. cp ./env.example ./.env
3. composer install
4. php artisan key:generate
5. npm install
6. npm run dev
7. php artisan migrate
8. Fill the database with either 'php artisan db:seed' or from the sql dump file
9. Pages:
    Task1: /masking
    Task2: /cash-flow
    Task3: /charts