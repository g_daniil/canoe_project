<?php

use Illuminate\Database\Seeder;

class InvestmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $invs = [
            ['client_id'=>1, 'fund_id'=>1, 'date'=>'2018-08-09 00:00:00', 'amount'=>100000],
            ['client_id'=>1, 'fund_id'=>2, 'date'=>'2018-08-09 00:00:00', 'amount'=>200000],
            ['client_id'=>1, 'fund_id'=>3, 'date'=>'2018-08-09 00:00:00', 'amount'=>300000],
            ['client_id'=>1, 'fund_id'=>4, 'date'=>'2018-08-09 00:00:00', 'amount'=>400000],

            ['client_id'=>2, 'fund_id'=>3, 'date'=>'2018-08-09 00:00:00', 'amount'=>100000],
            ['client_id'=>2, 'fund_id'=>4, 'date'=>'2018-08-09 00:00:00', 'amount'=>400000],
            ['client_id'=>2, 'fund_id'=>7, 'date'=>'2018-08-09 00:00:00', 'amount'=>700000],

            ['client_id'=>3, 'fund_id'=>2, 'date'=>'2018-08-09 00:00:00', 'amount'=>100000],
            ['client_id'=>3, 'fund_id'=>6, 'date'=>'2018-08-09 00:00:00', 'amount'=>500000],
        ];

        foreach($invs as $inv) {
            DB::table('investments')->insert($inv);
        }
    }
}
