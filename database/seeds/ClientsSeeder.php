<?php

use Illuminate\Database\Seeder;
use App\Client;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new Client([
            'name' => 'Client 1',
            'permission' => json_encode(['*']),
            'description' => str_random(100),
            'preference' => ''
        ]))->save();


        (new Client([
            'name' => 'Client 2',
            'permission' => json_encode(['VC', 'RE']),
            'description' => str_random(100),
            'preference' => ''
        ]))->save();


        (new Client([
            'name' => 'Client 3',
            'permission' => json_encode(['PL', 'PC']),
            'description' => str_random(100),
            'preference' => ''
        ]))->save();
    }
}
