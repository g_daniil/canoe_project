<?php

use Illuminate\Database\Seeder;
use App\Investment;
use App\CashFlow;

class CashFlowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $investments = Investment::all();
        foreach($investments as $inv) {
            for($i=0;$i<30;$i++){
                $cf = new CashFlow();
                $cf->investment_id = $inv->id;
                $cf->return = rand(-20, 30);
                $cf->date = \Carbon\Carbon::now()->addDays($i);
                $cf->save();
            }
        }
    }
}
