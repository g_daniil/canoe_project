<?php

use Illuminate\Database\Seeder;

class FundsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $funds = [
            ['name'=>'Fund 1', 'type'=>'HF', 'inception_date'=>'2018-08-08 00:00:00', 'description'=>str_random(100)],
            ['name'=>'Fund 2', 'type'=>'PL', 'inception_date'=>'2018-08-08 00:00:00', 'description'=>str_random(100)],
            ['name'=>'Fund 3', 'type'=>'VC', 'inception_date'=>'2018-08-08 00:00:00', 'description'=>str_random(100)],
            ['name'=>'Fund 4', 'type'=>'RE', 'inception_date'=>'2018-08-08 00:00:00', 'description'=>str_random(100)],
            ['name'=>'Fund 5', 'type'=>'PC', 'inception_date'=>'2018-08-08 00:00:00', 'description'=>str_random(100)],
            ['name'=>'Fund 6', 'type'=>'PL', 'inception_date'=>'2018-08-08 00:00:00', 'description'=>str_random(100)],
            ['name'=>'Fund 7', 'type'=>'VC', 'inception_date'=>'2018-08-08 00:00:00', 'description'=>str_random(100)],
            ['name'=>'Fund 8', 'type'=>'RE', 'inception_date'=>'2018-08-08 00:00:00', 'description'=>str_random(100)],
            ['name'=>'Fund 9', 'type'=>'PC', 'inception_date'=>'2018-08-08 00:00:00', 'description'=>str_random(100)],
        ];
        foreach($funds as $f) {
            DB::table('funds')->insert($f);
        }
    }
}
