<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('fund_id')->unsigned();
            $table->dateTime('date');
            $table->decimal('amount', 8, 2);
            $table->timestamps();

            $table->foreign('client_id')
            ->references('id')->on('clients')
            ->onDelete('cascade');

            $table->foreign('fund_id')
            ->references('id')->on('funds')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investments');
    }
}
